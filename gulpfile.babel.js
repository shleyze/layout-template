import requireDir from 'require-dir';

global.isProduction = process.env.NODE_ENV === 'production' ? true : false;


requireDir('./config/gulp/tasks/', { recurse: true });