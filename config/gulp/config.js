const src = './source';
const dev = './development';
const prod = './production';

module.exports = {
	path: {
		development: dev,
		production: prod
	},
	pug: {
		include: `${src}/pug/**/*.{pug,}`,
		exclude:  `!${src}/pug/**/*.ignore.{pug,}`,
		destination: '',
		pagesFolder: 'pages',
		settings: {
			pretty: true
		},
		message: {
			success: 'HTML compiled!',
			error: 'HTML filed!'
		},
	},
	less: {
		include: `${src}/less/**/*.{less,}`,
		exclude: `!${src}/less/**/*.ignore.{less,}`,
		destination: 'css',
		settings: {
		},
		addons: {
			autoprefixer: {
				browsers: ['last 20 version']
			},
			cssnano: {}
		},
		message: {
			success: 'CSS compiled!',
			error: 'CSS filed!'
		}
	},


// 	fonts: {
// 		include: source + '/fonts/**/*.ttf',
// 			exclude: '!' + source + '/less/**/*.ignore.less',
// 			messsage: {
// 				done: 'Fonts compiled!',
// 				error: 'Fonts filed!'
// 		},
// 		main: 'fonts.css',
// 		destination: root + '/fonts'
// 	},


};