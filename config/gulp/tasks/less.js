'use strict';
import gulp from 'gulp';
import ifElse from 'gulp-if-else';
import notifier from '../utils/notifier';

import {less as config} from '../config';
import {path} from '../config';

import sourcemaps from 'gulp-sourcemaps';
import less from 'gulp-less';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';

gulp.task('less', done => {
    gulp.src([config.include, config.exclude])
        .pipe(ifElse(!global.isProduction, () => sourcemaps.init()))
        .pipe(less(config.settings).on('error', function(error) {
            done(error);
            notifier(config.message.error);
        }))
        .pipe(postcss([
            autoprefixer(config.addons.autoprefixer)
        ]))
        .pipe(ifElse(global.isProduction, () => postcss([
            cssnano(config.addons.cssnano)
        ])))
        .pipe(ifElse(!global.isProduction, () => sourcemaps.write()))
        .pipe(gulp.dest(global.isProduction ? `${path.production}/${config.destination}`
            : `${path.development}/${config.destination}`))
        .on('end', () => {
            done();
            notifier(config.message.success);
        });
});
