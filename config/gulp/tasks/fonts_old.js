// 'use strict';
// import gulp from 'gulp';
// import {fonts as config} from '../config';
// import notifier from '../utils/notifier';
//
// import path from 'path';
// import edit from 'gulp-edit';
// import concat from 'gulp-concat';
// import fontmin from 'gulp-fontmin';
// import rebaseUrls from 'gulp-css-url-fix';
// import fontStyleKeywords from 'css-font-style-keywords';
// import fontWeightKeywords from 'css-font-weight-keywords';
// import fontWeightNames from 'css-font-weight-names';
// import each from 'gulp-each';
//
// function getFontStyle(basename) {
//     return basename
//         .split('-')
//         .slice(1)
//         .map(item => item.toLowerCase())
//         .reduce((prev, item) => {
//             if (fontStyleKeywords.indexOf(item) >= 0) {
//                 return `font-style: ${item}`
//             }
//             return prev
//         }, 'font-style: normal')
// }
//
// function getFontWeight(basename) {
//     return basename
//         .split('-')
//         .slice(1)
//         .map(item => item.toLowerCase())
//         .reduce((prev, item) => {
//             if (item === 'normal') {
//                 return prev
//             }
//
//             if (fontWeightNames[item]) {
//                 return `font-weight: ${fontWeightNames[item]}`
//             }
//
//             if (fontWeightKeywords.indexOf(item) >= 0) {
//                 return `font-weight: ${item}`
//             }
//
//             return prev
//         }, `font-weight: normal`)
// }
//
// gulp.task('fonts', ['fonts:css'], function () {
//     return notifier(config.messsage.done);
// });
// gulp.task('fonts:base', function () {
//     return gulp.src(config.include)
//         .pipe(fontmin())
//         .pipe(edit(function(src, cb){
//             var file = this,
//                 ext = path.extname(file.path),
//                 basename = path.basename(file.path, ext),
//                 fontWeightNormal = "font-weight: normal",
//                 fontWeightCorrect = getFontWeight(basename),
//                 fontStyleNormal = "font-style: normal",
//                 fontStyleCorrect = getFontStyle(basename);
//
//             if (ext === '.css') {
//                 src = src.replace(fontWeightNormal, fontWeightCorrect);
//                 src = src.replace(fontStyleNormal, fontStyleCorrect);
//             }
//
//             var err = null;
//             cb(err, src)
//         }))
//         .pipe(gulp.dest(config.destination));
// });
// gulp.task('fonts:css', ['fonts:base'], function () {
//     return gulp.src([config.destination + '/**/*.css', '!' + config.destination + '/' + config.main])
//         .pipe(rebaseUrls())
//         .pipe(concat(config.main))
//         .pipe(edit(function(src, cb){
//             src = src.replace(/build\/fonts\//g, '');
//             var err = null;
//             cb(err, src)
//         }))
//         .pipe(gulp.dest(config.destination));
// });