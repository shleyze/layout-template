'use strict';
import gulp from 'gulp';
import rename from 'gulp-rename';
import notifier from '../utils/notifier';

import {pug as config} from '../config';
import {path} from '../config';

import pug from 'gulp-pug';

gulp.task('pug', done => {
    gulp.src([config.include, config.exclude])
        .pipe(pug(config.settings).on('error', function(error) {
            done(error);
            notifier(config.message.error);
        }))
        .pipe(rename(path => {
            if(path.dirname.toLowerCase().indexOf(config.pagesFolder) >= 0) {
                path.dirname = "/";
            } else {
                path.dirname = "/"+ path.dirname.replace(/pug\//g, '');
            }
            return path;
        }))
        .pipe(gulp.dest(
            global.isProduction ? `${path.production}/${config.destination}`
                : `${path.development}/${config.destination}`
        ))
        .on('end', () => {
            done();
            notifier(config.message.success);
        });
});